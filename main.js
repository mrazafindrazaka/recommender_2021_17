const csv = require('csv-parser');
const fs = require('fs');

const MongoClient = require('mongodb').MongoClient;

const express = require('express')
const app = express()
const port = 3000

const stream = fs.createReadStream('KaDo.csv');

app.get('/family', (req, res) => {
    let data = [];

    stream.pipe(csv())
        .on('data', (row) => {
            if (data.indexOf(row.FAMILLE) === -1) {
                data.push(row.FAMILLE)
            }
        })
        .on('end', () => {
            res.send(data);
        });
});

app.get('/clients/:cli_id', (req, res) => {
    MongoClient.connect('mongodb://localhost:27017', function (err, client) {
        if (err) throw err;

        const db = client.db('recommender');

        db.collection('recommender').find({'CLI_ID': req.params.cli_id}).toArray(function (err, result) {
            if (err) throw err;
            res.send(result)
            client.close();
        });
    });
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})
